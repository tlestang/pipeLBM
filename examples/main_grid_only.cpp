#include <iostream>
#include "libpipeLBM.h"

using namespace std;

int main()
{
  int Dx = 513; int Dy = 129;
  int N = (Dx-1)/0.01;
  pipeLBM *myLB = new pipeLBM(Dx, Dy);
  myLB->setInletBC("poiseuille");
  myLB->setOutletBC("open");

  myLB->setSpongeLayer(Dx, Dx);

  int R = (Dy-1)/16; int x0 = (Dx-1)/16;
  Grid *myGrid = new Grid(x0, R, Dy);
  myLB->setObstacles(myGrid);
  
  myLB->initializeToEquilibrium();

  int k=0;
  for (int t=0;t<N;t++)
    {
      myLB->displayPercentage(t,N);
      if(t%5000==0)
	{
	  k++;
	  myLB->writeVTK(k);
	}
      myLB->advanceOneTimestep(myGrid);
    }
  delete myLB;
}
