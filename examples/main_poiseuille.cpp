#include <iostream>
#include "libpipeLBM.h"

using namespace std;

int main()
{
  int Dx = 64; int Dy = 16;
  int N = (Dx-1)/0.01;
  pipeLBM *myLB = new pipeLBM(Dx, Dy);
  myLB->setInletBC("poiseuille");
  myLB->setOutletBC("open");
  myLB->setSpongeLayer(Dx, Dx);
  myLB->initializeToEquilibrium();

  int k=0;
  for (int t=0;t<N;t++)
    {
      if(t%100==0)
	{
	  k++;
	  myLB->writeVTK(k);
	}
      myLB->advanceOneTimestep();
    }
  delete myLB;
}
