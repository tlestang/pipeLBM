#include <iostream>
#include <fstream>
#include "libpipeLBM.h"

using namespace std;

int main()
{
  int Dx = 257; int Dy = 65;

  pipeLBM *myLB = new pipeLBM(Dx, Dy);
  myLB->setInletBC("poiseuille");
  myLB->setOutletBC("open");
  
  int spongeStart = (int) 3*(Dx-1)/4+1;
  int R = (Dy-1)/16; int x0 = (Dx-1)/16;
  int L = 8; int xsq = (Dx-1)/2; int ysq = (Dy-1)/2 - L/2;
  double U0 = 0.05; double tau = 0.5005; double F0 = 0.0;
  //int N = 3*(Dx-1)/U0;
  int N = 200000;
  
  myLB->setSpongeLayer(spongeStart, Dx);
  myLB->setParameters(tau, U0, F0);

  Obstacle* obs[2];
  
  obs[0] = new Grid(x0, R, Dy);
  obs[1] = new squareObstacle(xsq, ysq, L);
  myLB->setObstacles(obs, 2);
      
  //myLB->initializeToEquilibrium();
  string path_to_file="/home/thibault/These/lbm_code/control_run_pipeLBM/control_run_low_res/run_for_pops/populations/";
  string root = "pops";
  bool errFlag;
  errFlag = myLB->initFromRandom(path_to_file, root, 5, 18);
  myLB->drawGeometry("geometry.dat");
  int k=0;
  double *f = new double[N];
  for (int t=0;t<N;t++)
    {
      myLB->displayPercentage(t,N);
      myLB->computeStress(obs[1]);
      f[t] = obs[1]->getDrag();
      myLB->advanceOneTimestep(obs, 2);
    }
  ofstream dragFile("dragFile.dat", ios::binary);
  dragFile.write((char*)&f[0], N*sizeof(double));
  dragFile.close();
  delete[] f;
  
  delete myLB;
}
