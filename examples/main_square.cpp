#include <iostream>
#include "libpipeLBM.h"
using namespace std;

int main()
{
  int Dx = 257; int Dy = 65; double U0 = 0.05;
  int N = (Dx-1)/U0;
  pipeLBM *myLB = new pipeLBM(Dx, Dy);
  myLB->setInletBC("poiseuille");
  myLB->setOutletBC("open");
  
  int spongeStart = (int) 3*(Dx-1)/4+1;
  myLB->setSpongeLayer(spongeStart, Dx);
  myLB->setParameters(0.501, U0, 0.0);
  
  int R = (Dx-1)/8; int x0 = (Dx-1)/8;
  int L = 8;
  squareObstacle *mySquare = new squareObstacle((Dx-1)/2, (Dy-1)/2 - L/2, L);
  myLB->setObstacles(mySquare);
    
  
  myLB->initializeToEquilibrium();

  int k=0;
  for (int t=0;t<N;t++)
    {
      myLB->displayPercentage(t,N);
      if(t%100==0)
	{
	  k++;
	  myLB->writeVTK(k);
	}
      myLB->advanceOneTimestep(mySquare);
    }
  delete myLB;
}
