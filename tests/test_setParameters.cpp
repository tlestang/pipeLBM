#include <iostream>
#include "libpipeLBM.h"

using namespace std;
int main()
{
  int Dx = 128; int Dy = 16;
  double tau = 0.05; double U = 0.01;
  double F0 = 0.0;
  int spgeStart = Dx/4; int spgeEnd = Dx;
  
  pipeLBM *myLB = new pipeLBM(Dx, Dy);
  myLB->setParameters(tau, U, F0);
  cout << "Utilisation de setParameters OK" << endl;
  myLB->setSpongeLayer(spgeStart, spgeEnd);
  cout << "Utilisation de setSpongeLayer OK" << endl;

  delete myLB;
}
