#include <iostream>
#include "libpipeLBM.h"

using namespace std;
int main()
{
  int x0 = 1, y0 = 2;
  int L = 32;
  squareObstacle *myObs = new squareObstacle(x0, y0, L);
  cout << "Instanciation de squareObstacle : OK " << endl;
  
  delete myObs;
  cout << "Destruction de squareObstacle : OK " << endl;
}
