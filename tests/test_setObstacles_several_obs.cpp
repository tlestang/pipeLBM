#include <iostream>
#include "libpipeLBM.h"

using namespace std;
int main()
{
  int Dx = 513; int Dy = 129;
  pipeLBM *myLB = new pipeLBM(Dx, Dy);
  
  int x0 = (Dx-1)/16; int Rgrid = 8;
  int L = 16;
  int xsq = (Dx-1)/2; int ysq = (Dy-1)/2 - L/2;

  Obstacle* obs[2];
  obs[0] = new Grid(x0,Rgrid,Dy);
  obs[1] = new squareObstacle(xsq, ysq, L);

  myLB->setObstacles(obs, 2);
  myLB->drawGeometry("geometrie.dat");

  delete obs[0];
  delete obs[1];
}
