#include <iostream>
#include "libpipeLBM.h"

using namespace std;
int main()
{
  int Dx = 128; int Dy = 16;
  pipeLBM *myLB = new pipeLBM(Dx, Dy);

  string methodID="poiseuille";
  myLB->setInletBC(methodID);
  (myLB->*(myLB->applyInletBC))();
  delete myLB;
}
