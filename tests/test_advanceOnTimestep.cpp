#include <iostream>
#include "libpipeLBM.h"

using namespace std;

int main()
{
  int Dx = 128; int Dy = 16;
  int R = 4;
  int x0 = Dx/2; int y0 = Dy/2 - R;
  pipeLBM *myLB = new pipeLBM(Dx, Dy);
  squareObstacle *myObs = new squareObstacle(x0,y0,R);

  myLB->setObstacles(*myObs);
  myLB->initializeToEquilibrium();

  myLB->advanceOneTimestep(*myObs);
 
  delete myLB;
  delete myObs;
}
