#include <iostream>
#include "libpipeLBM.h"

using namespace std;
int main()
{
  int Dx = 129;
  int x0 = (Dx-1)/4;
  int L = 4; int Dy = 65;
  Obstacle *myGrid = new Grid(x0, L, Dy);
  cout << "Instanciation de Grid : OK " << endl;
  
  delete myGrid;
  cout << "Destruction de Grid : OK " << endl;
}
