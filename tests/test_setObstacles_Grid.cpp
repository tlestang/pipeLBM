#include <iostream>
#include "libpipeLBM.h"

using namespace std;
int main()
{
  int Dx = 129; int Dy = 17;
  int R = 4;
  int x0 = (Dx-1)/4;
  pipeLBM *myLB = new pipeLBM(Dx, Dy);
  Obstacle *myGrid = new Grid(x0,R,Dy);

  myLB->setObstacles(myGrid);
  myLB->drawGeometry("geometrie.dat");

  delete myLB;
  delete myGrid;
}
