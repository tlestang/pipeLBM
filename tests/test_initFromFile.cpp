#include <iostream>
#include <fstream>
#include "libpipeLBM.h"

using namespace std;

int main()
{
  int Dx = 513; int Dy = 129;
  int N = 5000;
  pipeLBM *myLB = new pipeLBM(Dx, Dy);
  myLB->setInletBC("poiseuille");
  myLB->setOutletBC("open");
  
  int spongeStart = (int) 3*(Dx-1)/4+1;
  int R = (Dy-1)/16; int x0 = (Dx-1)/16;
  int L = 16; int xsq = (Dx-1)/2; int ysq = (Dy-1)/2 - L/2;
  double U0 = 0.05; double tau = 0.501; double F0 = 0.0;
				    
  myLB->setSpongeLayer(spongeStart, Dx);
  myLB->setParameters(tau, U0, F0);

  Obstacle* obs[2];
  squareObstacle mySquare(xsq, ysq, L);
  obs[0] = new Grid(x0, R, Dy);
  //  obs[1] = new squareObstacle(xsq, ysq, L);
  obs[1] = &mySquare;
  myLB->setObstacles(obs, 2);
  string path_to_file="/home/thibault/These/lbm_code/seq/draft/run_for_pops/populations/pops_2.datout";
  bool errFlag;
  errFlag = myLB->initFromFile(path_to_file);
  //myLB->initializeToEquilibrium();
  if(errFlag){cout << "PB with init" << endl;}

  int k=0;
  double *fd = new double[N];
  double *viscousRear = new double[N];
  double *viscousTop = new double[N];
  double *viscousFront = new double[N];
  double *viscousBot = new double[N];
  double *pRear = new double[N];
  double *pFront = new double[N];
  for (int t=0;t<N;t++)
    {
      myLB->displayPercentage(t,N);
      myLB->advanceOneTimestep(obs, 2);
      myLB->computeStress(obs[1]);
      //      fd[t] = myLB->getDrag(obs[1]);
      fd[t] = obs[1]->getDrag();
      viscousTop[t] = mySquare.getViscousTop();
      viscousBot[t] = mySquare.getViscousBot();
      viscousFront[t] = mySquare.getViscousFront();
      viscousRear[t] = mySquare.getViscousRear();
      pFront[t] = mySquare.getpFront();
      pRear[t] = mySquare.getpRear();
    }
  ofstream outputFile;
  outputFile.open("data_force.datout", ios::binary);
  outputFile.write((char*)&fd[0], N*sizeof(double));
  outputFile.close();
  outputFile.open("viscousTop.dat", ios::binary);
  outputFile.write((char*)&viscousTop[0], N*sizeof(double));
  outputFile.close();
  outputFile.open("viscousBot.dat", ios::binary);
  outputFile.write((char*)&viscousBot[0], N*sizeof(double));
  outputFile.close();
  outputFile.open("viscousRear.dat", ios::binary);
  outputFile.write((char*)&viscousRear[0], N*sizeof(double));
  outputFile.close();
  outputFile.open("viscousFront.dat", ios::binary);
  outputFile.write((char*)&viscousFront[0], N*sizeof(double));
  outputFile.close();
  outputFile.open("pFront.dat", ios::binary);
  outputFile.write((char*)&pFront[0], N*sizeof(double));
  outputFile.close();
  outputFile.open("pRear.dat", ios::binary);
  outputFile.write((char*)&pRear[0], N*sizeof(double));
  outputFile.close();
  delete[] fd;
  delete myLB;
}
