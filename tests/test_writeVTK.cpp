#include <iostream>
#include "libpipeLBM.h"

using namespace std;

int main()
{
  int Dx = 128; int Dy = 16;
  pipeLBM *myLB = new pipeLBM(Dx, Dy);
 
  myLB->initializeToEquilibrium();

  int t = 0;
  myLB->writeVTK(t);   

  delete myLB;
}
