#include <iostream>
#include <fstream>
#include "libpipeLBM.h"

using namespace std;

int main()
{
  int L = 32;
  int Dy = 4*L + 1; int Dx = 3*(Dy-1) + 1;
  int xmin1 = Dy-1; 
  int ymin1 = (Dy-1)/2 - L/2; 
  int xmin2 = 2*(Dy-1); 
  int ymin2 = (Dy-1)/2 - L/2; 

  int N = 5*(Dx-1)/0.03;
  pipeLBM *myLB = new pipeLBM(Dx, Dy);

  double tau = 0.5001;
  double U0 = 0.05;
  double F0 = (1./(Dx-1))*((double)L/(Dy-1))*U0*U0;;
  myLB->setParameters(tau, 0.0, F0);
  myLB->setSpongeLayer(Dx, Dx);
  Obstacle *obs[2];
  obs[0] = new squareObstacle(xmin1, ymin1, L);
  obs[1] = new squareObstacle(xmin2, ymin2, L);

  myLB->setObstacles(obs,2);
  myLB->initializeToEquilibrium();
  //int error = myLB->initFromFile("./tests/pops_2_sq_T_500.dat");
  //if(error){cout << "error" << endl;}
  //myLB->initMacro();

  int k=0;
  double *uxvec = new double[N];
  double *uyvec = new double[N];
  for (int t=0;t<N;t++)
    {
      if(t%500==0)
	{
	  myLB->writeVTK(t);
	}
      myLB->displayPercentage(t,N);
      uxvec[t] = myLB->getLongVelocityAtPoint(0,64);
      uyvec[t] = myLB->getTransVelocityAtPoint(0,64);
      //myLB->advanceOneTimestep_BGK(obs,2);
      myLB->advanceOneTimestep(obs,2);
    }

  ofstream ufile;
  ufile.open("ux.dat", ios::binary);
  ufile.write((char*)&uxvec[0], N*sizeof(double));
  ufile.close();
  ufile.open("uy.dat", ios::binary);
  ufile.write((char*)&uyvec[0], N*sizeof(double));
  ufile.close();
  delete myLB;
  delete obs[0];
  delete obs[1];
}
