#include <iostream>
#include "libpipeLBM.h"

using namespace std;

int main()
{
  int Dx = 513; int Dy = 129;
  int N = 3000;
  pipeLBM *myLB = new pipeLBM(Dx, Dy);
  myLB->setInletBC("poiseuille");
  myLB->setOutletBC("open");
  
  int spongeStart = (int) 3*(Dx-1)/4+1;
  int R = (Dy-1)/16; int x0 = (Dx-1)/16;
  int L = 16; int xsq = (Dx-1)/2; int ysq = (Dy-1)/2 - L/2;
  double U0 = 0.05; double tau = 0.501; double F0 = 0.0;
				    
  myLB->setSpongeLayer(spongeStart, Dx);
  myLB->setParameters(tau, U0, F0);

  Obstacle* obs[2];
  
  obs[0] = new Grid(x0, R, Dy);
  obs[1] = new squareObstacle(xsq, ysq, L);
  myLB->setObstacles(obs, 2);
  string path_to_file="/home/thibault/These/lbm_code/seq/draft/run_for_pops/populations/";
  string root = "pops";
  bool errFlag;
  errFlag = myLB->initFromRandom(path_to_file, root, 5, 20);
  if(errFlag){cout << "PB with init" << endl;}

  int k=0;
  for (int t=0;t<N;t++)
    {
      myLB->displayPercentage(t,N);
      if(t%100==0)
	{
	  k++;
	  myLB->writeVTK(k);
	}
      myLB->advanceOneTimestep(obs, 2);
    }
  delete myLB;
}
