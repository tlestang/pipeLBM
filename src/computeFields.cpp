#include "pipeLBM.h"

void pipeLBM::computeFieldsInBulk()
{
  int c[9][2] = {{0,0}, {1,0}, {0,1}, {-1,0}, {0,-1}, {1,1}, {-1,1}, {-1,-1}, {1,-1}};
  
  double rho_,ux_,uy_,ftemp;
  int y0;

  /*Compute in the bulk*/
  for(int x=1;x<m_Dx-1;x++)
    {
      for(int y=1;y<m_Dy-1;y++)
	{
	  if(isFluid[x][y])
	    {
	      rho_ = 0.0;
	      ux_ = uy_ = 0.0;
	      for (int k=0;k<9;k++)
		{
		  ftemp = fout[IDX(x,y,k)];
		  rho_ += ftemp;
		  ux_ += ftemp*c[k][0];
		  uy_ += ftemp*c[k][1];
		}
	      ux_ += 0.5*m_F0;
	      ux_ /= rho_; 
	      uy_ /= rho_;
	  
	      ux[idx(x,y)] = ux_; uy[idx(x,y)]=uy_;
	      rho[idx(x,y)] = rho_;
	    }
	}
	  
    }

  /*Compute along north and south walls, including corners*/
  y0=0;
  for(int x=0;x<m_Dx;x++)
    {
      rho_ = 0.0;
      ux_ = uy_ = 0.0;
      for (int k=0;k<9;k++)
	{
	  ftemp = fout[IDX(x,y0,k)];
	  rho_ += ftemp;
	  ux_ += ftemp*c[k][0];
	  uy_ += ftemp*c[k][1];
	}
      ux_ += 0.5*m_F0;
      ux_ /= rho_; 
      uy_ /= rho_;
	  
      ux[idx(x,y0)] = ux_; uy[idx(x,y0)]=uy_;
      rho[idx(x,y0)] = rho_;
    }

  y0=m_Dy-1;
  for(int x=0;x<m_Dx;x++)
    {
      rho_ = 0.0;
      ux_ = uy_ = 0.0;
      for (int k=0;k<9;k++)
	{
	  ftemp = fout[IDX(x,y0,k)];
	  rho_ += ftemp;
	  ux_ += ftemp*c[k][0];
	  uy_ += ftemp*c[k][1];
	}
      ux_ += 0.5*m_F0;
      ux_ /= rho_; 
      uy_ /= rho_;
      ux[idx(x,y0)] = ux_; uy[idx(x,y0)]=uy_;
      rho[idx(x,y0)] = rho_;
    }
}

void pipeLBM::computeFieldsAlongBorders()
{
  int c[9][2] = {{0,0}, {1,0}, {0,1}, {-1,0}, {0,-1}, {1,1}, {-1,1}, {-1,-1}, {1,-1}};
  double rho_,ux_,uy_,ftemp;
  int x0,y0;
  x0=0;
  for(int y=0;y<m_Dy;y++)
    {
      rho_ = 0.0;
      ux_ = uy_ = 0.0;
      for (int k=0;k<9;k++)
	{
	  ftemp = fout[IDX(x0,y,k)];
	  rho_ += ftemp;
	  ux_ += ftemp*c[k][0];
	  uy_ += ftemp*c[k][1];
	}
      ux_ += 0.5*m_F0;
      ux_ /= rho_; 
      uy_ /= rho_;
      ux[idx(x0,y)] = ux_; uy[idx(x0,y)]=uy_;
      rho[idx(x0,y)] = rho_;
    }

  x0=m_Dx-1;
  for(int y=0;y<m_Dy;y++)
    {
      rho_ = 0.0;
      ux_ = uy_ = 0.0;
      for (int k=0;k<9;k++)
	{
	  ftemp = fout[IDX(x0,y,k)];
	  rho_ += ftemp;
	  ux_ += ftemp*c[k][0];
	  uy_ += ftemp*c[k][1];
	}
      ux_ += 0.5*m_F0;
      ux_ /= rho_; 
      uy_ /= rho_;
	  
      ux[idx(x0,y)] = ux_; uy[idx(x0,y)]=uy_;
      rho[idx(x0,y)] = rho_;
    }
}
