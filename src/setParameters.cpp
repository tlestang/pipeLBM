#include "pipeLBM.h"
void pipeLBM::setParameters(double tau, double U, double F0)
{
  m_tau = tau;
  m_U = U;
  m_F0 = F0;
}
void pipeLBM::setSpongeLayer(int spgeStart, int spgeEnd)
{
  m_spgeStart = spgeStart;
  m_spgeEnd = spgeEnd;
}

