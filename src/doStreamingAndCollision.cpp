#include "pipeLBM.h"

void pipeLBM::doStreamingAndCollision()
{
  int c[9][2] = {{0,0}, {1,0}, {0,1}, {-1,0}, {0,-1}, {1,1}, {-1,1}, {-1,-1}, {1,-1}};
  double w[9]={4.0/9.0, 1.0/9.0, 1.0/9.0, 1.0/9.0, 1.0/9.0, 1.0/36.0, 1.0/36.0, 1.0/36.0, 1.0/36.0};
  
  double rho_, fk;
  double ux_, uy_; double omega = 1./m_tau; double eu, eueu, u2, feq;
  double omega1 = 1. - omega;
  double l, om, om1;
  int nx, ny;
  double k0,k3,k4,k5,k6,k7,k8;
  double k0_eq,k3_eq,k4_eq,k5_eq,k6_eq,k7_eq,k8_eq;
  double k3_neq,k4_neq,k5_neq;
  double k3_star,k4_star,k5_star;
  double CX, CY;
  
  for(int x=0;x<m_spgeStart;x++)
    {
      for(int y=0;y<m_Dy;y++)
	{
	  
	  if(isFluid[x][y])
	    {
	      ux_ = ux[idx(x,y)]; uy_ = uy[idx(x,y)];
	      rho_ = rho[idx(x,y)];

	      //k0 = rho_; k0_eq = rho_;
	      k3=k4=k5=k6=k7=k8=0.0;
	      k3_eq=k4_eq=k5_eq=0.0;
	      u2 = ux_*ux_ + uy_*uy_;
	      for(int k=0;k<9;k++)
		{
		  CX = c[k][0]-ux_; CY = c[k][1]-uy_;
		  eu = c[k][0]*ux_ + c[k][1]*uy_;
		  eueu = 4.5*eu*eu;
	      	      
		  fk = fin[IDX(x,y,k)];
		  /*Compute nonzero centered moments */
		  k3 += fk*(CX*CX+CY*CY);
		  k4 += fk*(CX*CX-CY*CY);
		  k5 += fk*CX*CY;
		  // k6 += fk*CX*CX*CY;
		  // k7 += fk*CX*CY*CY;
		  // k8 += fk*CX*CX*CY*CY;

		  /* Compute equilibrium centered moments*/
		  feq = w[k]*rho_*(1.0+3.0*eu+eueu-1.5*u2+4.5*(eu*eu*eu-eu*u2));
		  k3_eq += feq*(CX*CX+CY*CY);
		  k4_eq += feq*(CX*CX-CY*CY);
		  k5_eq += feq*CX*CY;
		  // k6_eq += feq*CX*CX*CY;
		  // k7_eq += feq*CX*CY*CY;
		  // k8_eq += feq*CX*CX*CY*CY;
		}

	      /*Collision for 2nd order centered moments*/
	      k3_neq = k3-k3_eq;
	      k4_neq = k4-k4_eq;
	      k5_neq = k5-k5_eq;
	      k3_star = k3_eq;// + omega1*k3_neq;
	      k4_star = k4_eq + omega1*k4_neq;
	      k5_star = k5_eq + omega1*k5_neq;
	  

	      nx = (x + c[0][0]+m_Dx)%m_Dx; ny = (y + c[0][1]+m_Dy)%m_Dy;
	      fout[IDX(nx,ny,0)]=(10*rho_)/9 - k3_star - rho_*ux_*ux_ - rho_*uy_*uy_ + (ux_*ux_*k3_star)/2 - (ux_*ux_*k4_star)/2 + (uy_*uy_*k3_star)/2 + (uy_*uy_*k4_star)/2 + 4*ux_*uy_*k5_star;

	      nx = (x + c[1][0]+m_Dx)%m_Dx; ny = (y + c[1][1]+m_Dy)%m_Dy;
	      fout[IDX(nx,ny,1)]=k3_star/4 - rho_/18 + k4_star/4 + (rho_*ux_)/2 - (ux_*k3_star)/4 + (ux_*k4_star)/4 - uy_*k5_star + (rho_*ux_*ux_)/2 - (ux_*ux_*k3_star)/4 + (ux_*ux_*k4_star)/4 - (uy_*uy_*k3_star)/4 - (uy_*uy_*k4_star)/4 - 2*ux_*uy_*k5_star - (rho_*ux_*uy_*uy_)/2;

	      nx = (x + c[2][0]+m_Dx)%m_Dx; ny = (y + c[2][1]+m_Dy)%m_Dy;
	      fout[IDX(nx,ny,2)] =k3_star/4 - rho_/18 - k4_star/4 + (rho_*uy_)/2 - ux_*k5_star - (uy_*k3_star)/4 - (uy_*k4_star)/4 + (rho_*uy_*uy_)/2 - (ux_*ux_*k3_star)/4 + (ux_*ux_*k4_star)/4 - (uy_*uy_*k3_star)/4 - (uy_*uy_*k4_star)/4 - 2*ux_*uy_*k5_star - (rho_*ux_*ux_*uy_)/2;

	      nx = (x + c[3][0]+m_Dx)%m_Dx; ny = (y + c[3][1]+m_Dy)%m_Dy;
	      fout[IDX(nx,ny,3)] = k3_star/4 - rho_/18 + k4_star/4 - (rho_*ux_)/2 + (ux_*k3_star)/4 - (ux_*k4_star)/4 + uy_*k5_star + (rho_*ux_*ux_)/2 - (ux_*ux_*k3_star)/4 + (ux_*ux_*k4_star)/4 - (uy_*uy_*k3_star)/4 - (uy_*uy_*k4_star)/4 - 2*ux_*uy_*k5_star + (rho_*ux_*uy_*uy_)/2;
	  
	      nx = (x + c[4][0]+m_Dx)%m_Dx; ny = (y + c[4][1]+m_Dy)%m_Dy;
	      fout[IDX(nx,ny,4)] = k3_star/4 - rho_/18 - k4_star/4 - (rho_*uy_)/2 + ux_*k5_star + (uy_*k3_star)/4 + (uy_*k4_star)/4 + (rho_*uy_*uy_)/2 - (ux_*ux_*k3_star)/4 + (ux_*ux_*k4_star)/4 - (uy_*uy_*k3_star)/4 - (uy_*uy_*k4_star)/4 - 2*ux_*uy_*k5_star + (rho_*ux_*ux_*uy_)/2;

	      nx = (x + c[5][0]+m_Dx)%m_Dx; ny = (y + c[5][1]+m_Dy)%m_Dy;
	      fout[IDX(nx,ny,5)] =rho_/36 + k5_star/4 + (ux_*k3_star)/8 - (ux_*k4_star)/8 + (ux_*k5_star)/2 + (uy_*k3_star)/8 + (uy_*k4_star)/8 + (uy_*k5_star)/2 + (ux_*ux_*k3_star)/8 - (ux_*ux_*k4_star)/8 + (uy_*uy_*k3_star)/8 + (uy_*uy_*k4_star)/8 + (rho_*ux_*uy_)/4 + ux_*uy_*k5_star + (rho_*ux_*uy_*uy_)/4 + (rho_*ux_*ux_*uy_)/4;

	      nx = (x + c[6][0]+m_Dx)%m_Dx; ny = (y + c[6][1]+m_Dy)%m_Dy;
	      fout[IDX(nx,ny,6)] =rho_/36 - k5_star/4 - (ux_*k3_star)/8 + (ux_*k4_star)/8 + (ux_*k5_star)/2 + (uy_*k3_star)/8 + (uy_*k4_star)/8 - (uy_*k5_star)/2 + (ux_*ux_*k3_star)/8 - (ux_*ux_*k4_star)/8 + (uy_*uy_*k3_star)/8 + (uy_*uy_*k4_star)/8 - (rho_*ux_*uy_)/4 + ux_*uy_*k5_star - (rho_*ux_*uy_*uy_)/4 + (rho_*ux_*ux_*uy_)/4;

	      nx = (x + c[7][0]+m_Dx)%m_Dx; ny = (y + c[7][1]+m_Dy)%m_Dy;
	      fout[IDX(nx,ny,7)] =rho_/36 + k5_star/4 - (ux_*k3_star)/8 + (ux_*k4_star)/8 - (ux_*k5_star)/2 - (uy_*k3_star)/8 - (uy_*k4_star)/8 - (uy_*k5_star)/2 + (ux_*ux_*k3_star)/8 - (ux_*ux_*k4_star)/8 + (uy_*uy_*k3_star)/8 + (uy_*uy_*k4_star)/8 + (rho_*ux_*uy_)/4 + ux_*uy_*k5_star - (rho_*ux_*uy_*uy_)/4 - (rho_*ux_*ux_*uy_)/4;

	      nx = (x + c[8][0]+m_Dx)%m_Dx; ny = (y + c[8][1]+m_Dy)%m_Dy;
	      fout[IDX(nx,ny,8)] = rho_/36 - k5_star/4 + (ux_*k3_star)/8 - (ux_*k4_star)/8 - (ux_*k5_star)/2 - (uy_*k3_star)/8 - (uy_*k4_star)/8 + (uy_*k5_star)/2 + (ux_*ux_*k3_star)/8 - (ux_*ux_*k4_star)/8 + (uy_*uy_*k3_star)/8 + (uy_*uy_*k4_star)/8 - (rho_*ux_*uy_)/4 + ux_*uy_*k5_star + (rho_*ux_*uy_*uy_)/4 - (rho_*ux_*ux_*uy_)/4;

	    }
	}
    }


  for(int x=m_spgeStart;x<m_spgeEnd;x++)
    {
      l = (x-m_spgeStart)/(double)(m_spgeEnd-1 - m_spgeStart);
      om = (1-0.999*l*l)*omega;
      om1 = 1-om;
      for(int y=0;y<m_Dy;y++)
	{

	  if(isFluid[x][y])
	    {
	      ux_ = ux[idx(x,y)]; uy_ = uy[idx(x,y)];
	      rho_ = rho[idx(x,y)];

	      //k0 = rho_; k0_eq = rho_;
	      k3=k4=k5=k6=k7=k8=0.0;
	      k3_eq=k4_eq=k5_eq=0.0;
	      u2 = ux_*ux_ + uy_*uy_;
	      for(int k=0;k<9;k++)
		{
		  CX = c[k][0]-ux_; CY = c[k][1]-uy_;
		  eu = c[k][0]*ux_ + c[k][1]*uy_;
		  eueu = 4.5*eu*eu;
	      	      
		  fk = fin[IDX(x,y,k)];
		  /*Compute nonzero centered moments */
		  k3 += fk*(CX*CX+CY*CY);
		  k4 += fk*(CX*CX-CY*CY);
		  k5 += fk*CX*CY;
		  // k6 += fk*CX*CX*CY;
		  // k7 += fk*CX*CY*CY;
		  // k8 += fk*CX*CX*CY*CY;

		  /* Compute equilibrium centered moments*/
		  feq = w[k]*rho_*(1.0+3.0*eu+eueu-1.5*u2+4.5*(eu*eu*eu-eu*u2));
		  k3_eq += feq*(CX*CX+CY*CY);
		  k4_eq += feq*(CX*CX-CY*CY);
		  k5_eq += feq*CX*CY;
		  // k6_eq += feq*CX*CX*CY;
		  // k7_eq += feq*CX*CY*CY;
		  // k8_eq += feq*CX*CX*CY*CY;
		}

	      /*Collision for 2nd order centered moments*/
	      k3_neq = k3-k3_eq;
	      k4_neq = k4-k4_eq;
	      k5_neq = k5-k5_eq;
	      k3_star = k3_eq;// + omega1*k3_neq;
	      k4_star = k4_eq + om1*k4_neq;
	      k5_star = k5_eq + om1*k5_neq;
	  

	      nx = (x + c[0][0]+m_Dx)%m_Dx; ny = (y + c[0][1]+m_Dy)%m_Dy;
	      fout[IDX(nx,ny,0)]=(10*rho_)/9 - k3_star - rho_*ux_*ux_ - rho_*uy_*uy_ + (ux_*ux_*k3_star)/2 - (ux_*ux_*k4_star)/2 + (uy_*uy_*k3_star)/2 + (uy_*uy_*k4_star)/2 + 4*ux_*uy_*k5_star;

	      nx = (x + c[1][0]+m_Dx)%m_Dx; ny = (y + c[1][1]+m_Dy)%m_Dy;
	      fout[IDX(nx,ny,1)]=k3_star/4 - rho_/18 + k4_star/4 + (rho_*ux_)/2 - (ux_*k3_star)/4 + (ux_*k4_star)/4 - uy_*k5_star + (rho_*ux_*ux_)/2 - (ux_*ux_*k3_star)/4 + (ux_*ux_*k4_star)/4 - (uy_*uy_*k3_star)/4 - (uy_*uy_*k4_star)/4 - 2*ux_*uy_*k5_star - (rho_*ux_*uy_*uy_)/2;

	      nx = (x + c[2][0]+m_Dx)%m_Dx; ny = (y + c[2][1]+m_Dy)%m_Dy;
	      fout[IDX(nx,ny,2)] =k3_star/4 - rho_/18 - k4_star/4 + (rho_*uy_)/2 - ux_*k5_star - (uy_*k3_star)/4 - (uy_*k4_star)/4 + (rho_*uy_*uy_)/2 - (ux_*ux_*k3_star)/4 + (ux_*ux_*k4_star)/4 - (uy_*uy_*k3_star)/4 - (uy_*uy_*k4_star)/4 - 2*ux_*uy_*k5_star - (rho_*ux_*ux_*uy_)/2;

	      nx = (x + c[3][0]+m_Dx)%m_Dx; ny = (y + c[3][1]+m_Dy)%m_Dy;
	      fout[IDX(nx,ny,3)] = k3_star/4 - rho_/18 + k4_star/4 - (rho_*ux_)/2 + (ux_*k3_star)/4 - (ux_*k4_star)/4 + uy_*k5_star + (rho_*ux_*ux_)/2 - (ux_*ux_*k3_star)/4 + (ux_*ux_*k4_star)/4 - (uy_*uy_*k3_star)/4 - (uy_*uy_*k4_star)/4 - 2*ux_*uy_*k5_star + (rho_*ux_*uy_*uy_)/2;
	  
	      nx = (x + c[4][0]+m_Dx)%m_Dx; ny = (y + c[4][1]+m_Dy)%m_Dy;
	      fout[IDX(nx,ny,4)] = k3_star/4 - rho_/18 - k4_star/4 - (rho_*uy_)/2 + ux_*k5_star + (uy_*k3_star)/4 + (uy_*k4_star)/4 + (rho_*uy_*uy_)/2 - (ux_*ux_*k3_star)/4 + (ux_*ux_*k4_star)/4 - (uy_*uy_*k3_star)/4 - (uy_*uy_*k4_star)/4 - 2*ux_*uy_*k5_star + (rho_*ux_*ux_*uy_)/2;

	      nx = (x + c[5][0]+m_Dx)%m_Dx; ny = (y + c[5][1]+m_Dy)%m_Dy;
	      fout[IDX(nx,ny,5)] =rho_/36 + k5_star/4 + (ux_*k3_star)/8 - (ux_*k4_star)/8 + (ux_*k5_star)/2 + (uy_*k3_star)/8 + (uy_*k4_star)/8 + (uy_*k5_star)/2 + (ux_*ux_*k3_star)/8 - (ux_*ux_*k4_star)/8 + (uy_*uy_*k3_star)/8 + (uy_*uy_*k4_star)/8 + (rho_*ux_*uy_)/4 + ux_*uy_*k5_star + (rho_*ux_*uy_*uy_)/4 + (rho_*ux_*ux_*uy_)/4;

	      nx = (x + c[6][0]+m_Dx)%m_Dx; ny = (y + c[6][1]+m_Dy)%m_Dy;
	      fout[IDX(nx,ny,6)] =rho_/36 - k5_star/4 - (ux_*k3_star)/8 + (ux_*k4_star)/8 + (ux_*k5_star)/2 + (uy_*k3_star)/8 + (uy_*k4_star)/8 - (uy_*k5_star)/2 + (ux_*ux_*k3_star)/8 - (ux_*ux_*k4_star)/8 + (uy_*uy_*k3_star)/8 + (uy_*uy_*k4_star)/8 - (rho_*ux_*uy_)/4 + ux_*uy_*k5_star - (rho_*ux_*uy_*uy_)/4 + (rho_*ux_*ux_*uy_)/4;

	      nx = (x + c[7][0]+m_Dx)%m_Dx; ny = (y + c[7][1]+m_Dy)%m_Dy;
	      fout[IDX(nx,ny,7)] =rho_/36 + k5_star/4 - (ux_*k3_star)/8 + (ux_*k4_star)/8 - (ux_*k5_star)/2 - (uy_*k3_star)/8 - (uy_*k4_star)/8 - (uy_*k5_star)/2 + (ux_*ux_*k3_star)/8 - (ux_*ux_*k4_star)/8 + (uy_*uy_*k3_star)/8 + (uy_*uy_*k4_star)/8 + (rho_*ux_*uy_)/4 + ux_*uy_*k5_star - (rho_*ux_*uy_*uy_)/4 - (rho_*ux_*ux_*uy_)/4;

	      nx = (x + c[8][0]+m_Dx)%m_Dx; ny = (y + c[8][1]+m_Dy)%m_Dy;
	      fout[IDX(nx,ny,8)] = rho_/36 - k5_star/4 + (ux_*k3_star)/8 - (ux_*k4_star)/8 - (ux_*k5_star)/2 - (uy_*k3_star)/8 - (uy_*k4_star)/8 + (uy_*k5_star)/2 + (ux_*ux_*k3_star)/8 - (ux_*ux_*k4_star)/8 + (uy_*uy_*k3_star)/8 + (uy_*uy_*k4_star)/8 - (rho_*ux_*uy_)/4 + ux_*uy_*k5_star + (rho_*ux_*uy_*uy_)/4 - (rho_*ux_*ux_*uy_)/4;

	    }
	}
    }

    for(int x=m_spgeEnd;x<m_Dx;x++)
    {
      om = 0.001*omega;
      om1 = 1-om;
      for(int y=0;y<m_Dy;y++)
	{

	  if(isFluid[x][y])
	    {
	      ux_ = ux[idx(x,y)]; uy_ = uy[idx(x,y)];
	      rho_ = rho[idx(x,y)];

	      //k0 = rho_; k0_eq = rho_;
	      k3=k4=k5=k6=k7=k8=0.0;
	      k3_eq=k4_eq=k5_eq=0.0;
	      u2 = ux_*ux_ + uy_*uy_;
	      for(int k=0;k<9;k++)
		{
		  CX = c[k][0]-ux_; CY = c[k][1]-uy_;
		  eu = c[k][0]*ux_ + c[k][1]*uy_;
		  eueu = 4.5*eu*eu;
	      	      
		  fk = fin[IDX(x,y,k)];
		  /*Compute nonzero centered moments */
		  k3 += fk*(CX*CX+CY*CY);
		  k4 += fk*(CX*CX-CY*CY);
		  k5 += fk*CX*CY;
		  // k6 += fk*CX*CX*CY;
		  // k7 += fk*CX*CY*CY;
		  // k8 += fk*CX*CX*CY*CY;

		  /* Compute equilibrium centered moments*/
		  feq = w[k]*rho_*(1.0+3.0*eu+eueu-1.5*u2+4.5*(eu*eu*eu-eu*u2));
		  k3_eq += feq*(CX*CX+CY*CY);
		  k4_eq += feq*(CX*CX-CY*CY);
		  k5_eq += feq*CX*CY;
		  // k6_eq += feq*CX*CX*CY;
		  // k7_eq += feq*CX*CY*CY;
		  // k8_eq += feq*CX*CX*CY*CY;
		}

	      /*Collision for 2nd order centered moments*/
	      k3_neq = k3-k3_eq;
	      k4_neq = k4-k4_eq;
	      k5_neq = k5-k5_eq;
	      k3_star = k3_eq;// + omega1*k3_neq;
	      k4_star = k4_eq + om1*k4_neq;
	      k5_star = k5_eq + om1*k5_neq;
	  

	      nx = (x + c[0][0]+m_Dx)%m_Dx; ny = (y + c[0][1]+m_Dy)%m_Dy;
	      fout[IDX(nx,ny,0)]=(10*rho_)/9 - k3_star - rho_*ux_*ux_ - rho_*uy_*uy_ + (ux_*ux_*k3_star)/2 - (ux_*ux_*k4_star)/2 + (uy_*uy_*k3_star)/2 + (uy_*uy_*k4_star)/2 + 4*ux_*uy_*k5_star;

	      nx = (x + c[1][0]+m_Dx)%m_Dx; ny = (y + c[1][1]+m_Dy)%m_Dy;
	      fout[IDX(nx,ny,1)]=k3_star/4 - rho_/18 + k4_star/4 + (rho_*ux_)/2 - (ux_*k3_star)/4 + (ux_*k4_star)/4 - uy_*k5_star + (rho_*ux_*ux_)/2 - (ux_*ux_*k3_star)/4 + (ux_*ux_*k4_star)/4 - (uy_*uy_*k3_star)/4 - (uy_*uy_*k4_star)/4 - 2*ux_*uy_*k5_star - (rho_*ux_*uy_*uy_)/2;

	      nx = (x + c[2][0]+m_Dx)%m_Dx; ny = (y + c[2][1]+m_Dy)%m_Dy;
	      fout[IDX(nx,ny,2)] =k3_star/4 - rho_/18 - k4_star/4 + (rho_*uy_)/2 - ux_*k5_star - (uy_*k3_star)/4 - (uy_*k4_star)/4 + (rho_*uy_*uy_)/2 - (ux_*ux_*k3_star)/4 + (ux_*ux_*k4_star)/4 - (uy_*uy_*k3_star)/4 - (uy_*uy_*k4_star)/4 - 2*ux_*uy_*k5_star - (rho_*ux_*ux_*uy_)/2;

	      nx = (x + c[3][0]+m_Dx)%m_Dx; ny = (y + c[3][1]+m_Dy)%m_Dy;
	      fout[IDX(nx,ny,3)] = k3_star/4 - rho_/18 + k4_star/4 - (rho_*ux_)/2 + (ux_*k3_star)/4 - (ux_*k4_star)/4 + uy_*k5_star + (rho_*ux_*ux_)/2 - (ux_*ux_*k3_star)/4 + (ux_*ux_*k4_star)/4 - (uy_*uy_*k3_star)/4 - (uy_*uy_*k4_star)/4 - 2*ux_*uy_*k5_star + (rho_*ux_*uy_*uy_)/2;
	  
	      nx = (x + c[4][0]+m_Dx)%m_Dx; ny = (y + c[4][1]+m_Dy)%m_Dy;
	      fout[IDX(nx,ny,4)] = k3_star/4 - rho_/18 - k4_star/4 - (rho_*uy_)/2 + ux_*k5_star + (uy_*k3_star)/4 + (uy_*k4_star)/4 + (rho_*uy_*uy_)/2 - (ux_*ux_*k3_star)/4 + (ux_*ux_*k4_star)/4 - (uy_*uy_*k3_star)/4 - (uy_*uy_*k4_star)/4 - 2*ux_*uy_*k5_star + (rho_*ux_*ux_*uy_)/2;

	      nx = (x + c[5][0]+m_Dx)%m_Dx; ny = (y + c[5][1]+m_Dy)%m_Dy;
	      fout[IDX(nx,ny,5)] =rho_/36 + k5_star/4 + (ux_*k3_star)/8 - (ux_*k4_star)/8 + (ux_*k5_star)/2 + (uy_*k3_star)/8 + (uy_*k4_star)/8 + (uy_*k5_star)/2 + (ux_*ux_*k3_star)/8 - (ux_*ux_*k4_star)/8 + (uy_*uy_*k3_star)/8 + (uy_*uy_*k4_star)/8 + (rho_*ux_*uy_)/4 + ux_*uy_*k5_star + (rho_*ux_*uy_*uy_)/4 + (rho_*ux_*ux_*uy_)/4;

	      nx = (x + c[6][0]+m_Dx)%m_Dx; ny = (y + c[6][1]+m_Dy)%m_Dy;
	      fout[IDX(nx,ny,6)] =rho_/36 - k5_star/4 - (ux_*k3_star)/8 + (ux_*k4_star)/8 + (ux_*k5_star)/2 + (uy_*k3_star)/8 + (uy_*k4_star)/8 - (uy_*k5_star)/2 + (ux_*ux_*k3_star)/8 - (ux_*ux_*k4_star)/8 + (uy_*uy_*k3_star)/8 + (uy_*uy_*k4_star)/8 - (rho_*ux_*uy_)/4 + ux_*uy_*k5_star - (rho_*ux_*uy_*uy_)/4 + (rho_*ux_*ux_*uy_)/4;

	      nx = (x + c[7][0]+m_Dx)%m_Dx; ny = (y + c[7][1]+m_Dy)%m_Dy;
	      fout[IDX(nx,ny,7)] =rho_/36 + k5_star/4 - (ux_*k3_star)/8 + (ux_*k4_star)/8 - (ux_*k5_star)/2 - (uy_*k3_star)/8 - (uy_*k4_star)/8 - (uy_*k5_star)/2 + (ux_*ux_*k3_star)/8 - (ux_*ux_*k4_star)/8 + (uy_*uy_*k3_star)/8 + (uy_*uy_*k4_star)/8 + (rho_*ux_*uy_)/4 + ux_*uy_*k5_star - (rho_*ux_*uy_*uy_)/4 - (rho_*ux_*ux_*uy_)/4;

	      nx = (x + c[8][0]+m_Dx)%m_Dx; ny = (y + c[8][1]+m_Dy)%m_Dy;
	      fout[IDX(nx,ny,8)] = rho_/36 - k5_star/4 + (ux_*k3_star)/8 - (ux_*k4_star)/8 - (ux_*k5_star)/2 - (uy_*k3_star)/8 - (uy_*k4_star)/8 + (uy_*k5_star)/2 + (ux_*ux_*k3_star)/8 - (ux_*ux_*k4_star)/8 + (uy_*uy_*k3_star)/8 + (uy_*uy_*k4_star)/8 - (rho_*ux_*uy_)/4 + ux_*uy_*k5_star + (rho_*ux_*uy_*uy_)/4 - (rho_*ux_*ux_*uy_)/4;

	    }
	}
    }
    
}
