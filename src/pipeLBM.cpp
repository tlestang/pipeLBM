#include <cmath>
#include <iostream>
#include <fstream>
#include "pipeLBM.h"

using namespace std;

pipeLBM::pipeLBM(int Dx, int Dy) : m_Dx(Dx), m_Dy(Dy)
{
  fin = new double[m_Dx*m_Dy*9];
  fout = new double[m_Dx*m_Dy*9];
  ux = new double[m_Dx*m_Dy];
  uy = new double[m_Dx*m_Dy];
  rho = new double[m_Dx*m_Dy];

  /*isFluid denotes fluid nodes from solid nodes
    isFluid[x][y]==false indicates that nodes (x,y) is a solid node */
  isFluid = new bool*[m_Dx];
  for(int x=0;x<m_Dx;x++)
    {
      isFluid[x] = new bool[m_Dy];
      for(int y=0;y<m_Dy;y++){isFluid[x][y] = true;}
    }

  /*Initialize parameters to default values*/
  /*Default parameters lead to the simulation of a Poiseuille flow with
    Re = 1 and Ma = 0.01*/
  m_U = 0.01/sqrt(3); //Ma = U/cs^2 with cs = 1/sqrt(3)
  m_tau = 3.*m_U*(m_Dy-1) + 0.5;
  m_F0 = 0.0;
  m_percent=0;

  /*If the user does not prescribe any method for boundary conditions
    then the flow is periodic along the channel direction*/
  applyInletBC = &pipeLBM::applyInletPeriodic;
  applyOutletBC = &pipeLBM::applyOutletPeriodic;

  /*Opens file in order to write snapshots on disk*/
  /*i.e. ux, uy and rho arrays.*/
  m_uxFile.open("ux.dat", ios::binary | ios::app);
  m_uyFile.open("uy.dat", ios::binary | ios::app);
  m_rhoFile.open("rho.dat", ios::binary | ios::app);

}

pipeLBM::~pipeLBM()
{
  delete[] fin;
  delete[] fout;
  delete[] rho;
  delete[] ux;
  delete[] uy;
  for(int x=0;x<m_Dx;x++){delete[] isFluid[x];}
  delete[] isFluid;
  m_uxFile.close();
  m_uyFile.close();
  m_rhoFile.close();

}

void pipeLBM::setObstacles(Obstacle *obs)
{
  obs->markSolidNodes(isFluid);
}
void pipeLBM::setObstacles(Obstacle *obs[], int nbObs)
{
  for(int i=0;i<nbObs;i++)
    {
      obs[i]->markSolidNodes(isFluid);
    }
}
void pipeLBM::drawGeometry(const char* fileName)
{
  ofstream geometryFile(fileName, ios::binary);
  for(int x=0;x<m_Dx;x++)
    {
      geometryFile.write((char*)isFluid[x], m_Dy*sizeof(bool));
    }
  geometryFile.close();
}
bool pipeLBM::setInletBC(string methodID)
{
  bool errorFlag=false;
  string openInletID = "poiseuille";
  if(methodID == openInletID)
    {
      applyInletBC = &pipeLBM::applyInletPoiseuille;
    }
  else
    {
      cout << "ERROR in pipeLBM::setInletBC(string) : Incorrect method identifier" << endl;
      errorFlag=true;
    }
  return errorFlag;
}
bool pipeLBM::setOutletBC(string methodID)
{
  bool errorFlag=false;
  string openOutletID = "open";
  if(methodID == openOutletID)
    {
      applyOutletBC = &pipeLBM::applyOutletOpen;
    }
  else
    {
      cout << "ERROR in pipeLBM::setOutletBC(string) : Incorrect method identifier" << endl;
      errorFlag=true;
    }
  return errorFlag;
}
void pipeLBM::initializeToEquilibrium()
{
  int c[9][2] = {{0,0}, {1,0}, {0,1}, {-1,0}, {0,-1}, {1,1}, {-1,1}, {-1,-1}, {1,-1}};
  double w[9]={4.0/9.0, 1.0/9.0, 1.0/9.0, 1.0/9.0, 1.0/9.0, 1.0/36.0, 1.0/36.0, 1.0/36.0, 1.0/36.0};
  for(int x=0;x<m_Dx;x++)
    {
      for(int y=0;y<m_Dy;y++)
	{
	  for(int k=0;k<9;k++)
	    {
	      fin[IDX(x,y,k)] = w[k];
	    }
	  ux[idx(x,y)] = 0.0;
	  uy[idx(x,y)] = 0.0;
	  rho[idx(x,y)] = 1.0;
	}
    }
}
void pipeLBM::computeStress(Obstacle *obs)
{
  obs->computeStress(fin, m_Dy, m_tau);
}
double* pipeLBM::getState()
{
  return fin;
}
double* pipeLBM::getUx()
{
  return ux;
}
double* pipeLBM::getUy()
{
  return uy;
}
void pipeLBM::advanceOneTimestep()
{
  /*In the case there no obstacle*/
  double *temp;
  
  doStreamingAndCollision();
  applyWallsBounceBack();
  computeFieldsInBulk();
  (this->*applyInletBC)();
  (this->*applyOutletBC)();
  computeFieldsAlongBorders();

  temp = fin;
  fin = fout;
  fout = temp;
}
void pipeLBM::advanceOneTimestep(Obstacle *obs)
{
  /*In the case there is 1 obstacle*/
  double *temp;
  
  doStreamingAndCollision();
  applyWallsBounceBack();
  obs->applyBoundary(fin,fout,m_Dy);
  computeFieldsInBulk();
  (this->*applyInletBC)();
  (this->*applyOutletBC)();
  computeFieldsAlongBorders();

  temp = fin;
  fin = fout;
  fout = temp;
}
void pipeLBM::advanceOneTimestep(Obstacle *obs[], int nbObs)
{
  /*In the case there are several obstacles*/
  double *temp;
  
  doStreamingAndCollision();
  applyWallsBounceBack();
  for(int i=0;i<nbObs;i++)
    {
      obs[i]->applyBoundary(fin,fout,m_Dy);
    }
  computeFieldsInBulk();
  (this->*applyInletBC)();
  (this->*applyOutletBC)();
  computeFieldsAlongBorders();

  temp = fin;
  fin = fout;
  fout = temp;
}
void pipeLBM::advanceOneTimestep_parallel(int nbProcs)
{
  /*In the case there no obstacle*/
  double *temp;
  
  doStreamingAndCollision_parallel(nbProcs);
  applyWallsBounceBack();
  computeFieldsInBulk();
  (this->*applyInletBC)();
  (this->*applyOutletBC)();
  computeFieldsAlongBorders();

  temp = fin;
  fin = fout;
  fout = temp;
}
void pipeLBM::advanceOneTimestep_parallel(Obstacle *obs, int nbProcs)
{
  /*In the case there is 1 obstacle*/
  double *temp;
  
  doStreamingAndCollision_parallel(nbProcs);
  applyWallsBounceBack();
  obs->applyBoundary(fin,fout,m_Dy);
  computeFieldsInBulk();
  (this->*applyInletBC)();
  (this->*applyOutletBC)();
  computeFieldsAlongBorders();

  temp = fin;
  fin = fout;
  fout = temp;
}
void pipeLBM::advanceOneTimestep_parallel(Obstacle *obs[], int nbObs, int nbProcs)
{
  /*In the case there are several obstacles*/
  double *temp;
  
  doStreamingAndCollision_parallel(nbProcs);
  applyWallsBounceBack();
  for(int i=0;i<nbObs;i++)
    {
      obs[i]->applyBoundary(fin,fout,m_Dy);
    }
  computeFieldsInBulk();
  (this->*applyInletBC)();
  (this->*applyOutletBC)();
  computeFieldsAlongBorders();

  temp = fin;
  fin = fout;
  fout = temp;
}
void pipeLBM::advanceOneTimestep_BGK()
{
  /*In the case there no obstacle*/
  double *temp;

  doStreamingAndCollision_BGK();
  applyWallsBounceBack();
  computeFieldsInBulk();
  (this->*applyInletBC)();
  (this->*applyOutletBC)();
  computeFieldsAlongBorders();

  temp = fin;
  fin = fout;
  fout = temp;
}
void pipeLBM::advanceOneTimestep_BGK(Obstacle *obs)
{
  /*In the case there is 1 obstacle*/
  double *temp;
  
  doStreamingAndCollision_BGK();
  applyWallsBounceBack();
  obs->applyBoundary(fin,fout,m_Dy);
  computeFieldsInBulk();
  (this->*applyInletBC)();
  (this->*applyOutletBC)();
  computeFieldsAlongBorders();

  temp = fin;
  fin = fout;
  fout = temp;
}
void pipeLBM::advanceOneTimestep_BGK(Obstacle *obs[], int nbObs)
{
  /*In the case there are several obstacles*/
  double *temp;

  doStreamingAndCollision_BGK();
  applyWallsBounceBack();
  for(int i=0;i<nbObs;i++)
    {
      obs[i]->applyBoundary(fin,fout,m_Dy);
    }
  computeFieldsInBulk();
  (this->*applyInletBC)();
  (this->*applyOutletBC)();
  computeFieldsAlongBorders();

  temp = fin;
  fin = fout;
  fout = temp;
}
void pipeLBM::applyWallsBounceBack()
{
  /*Apply halfway BB to north and south wall, including corners*/
  for(int x=0;x<m_Dx;x++)
    {
      /*North boundary*/
      fout[IDX(x,m_Dy-1,4)] = fin[IDX(x,m_Dy-1,2)];
      fout[IDX(x,m_Dy-1,8)] = fin[IDX(x,m_Dy-1,6)];
      fout[IDX(x,m_Dy-1,7)] = fin[IDX(x,m_Dy-1,5)];

      /*South boundary*/
      fout[IDX(x,0,2)] = fin[IDX(x,0,4)];
      fout[IDX(x,0,5)] = fin[IDX(x,0,7)];
      fout[IDX(x,0,6)] = fin[IDX(x,0,8)];
    }
  /*South East Corner*/
  fout[IDX(m_Dx-1,0,3)] = fin[IDX(m_Dx-1,0,1)];
  fout[IDX(m_Dx-1,0,7)] = fin[IDX(m_Dx-1,0,5)];
  /*North East Corner*/
  fout[IDX(m_Dx-1,m_Dy-1,3)] = fin[IDX(m_Dx-1,m_Dy-1,1)];
  fout[IDX(m_Dx-1,m_Dy-1,6)] = fin[IDX(m_Dx-1,m_Dy-1,8)];
 }
void pipeLBM::applyInletPeriodic()
{
}
void pipeLBM::applyOutletPeriodic()
{
}
void pipeLBM::displayPercentage(int currentIteration, int totalNbOfIterations)
{
  if(currentIteration % (totalNbOfIterations / 100) == 0)
    {
      m_percent++;
      cout << m_percent <<"%\r"; fflush(stdout);
    }
}
double pipeLBM::getLongVelocityAtPoint(int x, int y)
{
  return ux[idx(x,y)];
}
double pipeLBM::getTransVelocityAtPoint(int x, int y)
{
  return uy[idx(x,y)];
}
double pipeLBM::getVorticityAtPoint(int x, int y)
{
  double dxy, dyx;
  dxy = 0.5*(uy[idx(x+1,y)]-uy[idx(x-1,y)]);
  dyx = 0.5*(ux[idx(x,y+1)]-ux[idx(x,y-1)]);

  return dxy-dyx;
}
double pipeLBM::getDensityAtPoint(int x, int y)
{
  return rho[idx(x,y)];
}
void pipeLBM::writeSnapshot()
{
  int N = m_Dx*m_Dy;
  m_uxFile.write((char*)&ux[0], N*sizeof(double));
  m_uyFile.write((char*)&uy[0], N*sizeof(double));
  m_rhoFile.write((char*)&rho[0], N*sizeof(double));
}

  

