#ifndef _OBSTACLE
#define _OBSTACLE

#define IDX_OBS(i,j,k) ((k) + 9*( (j)+ Dy*(i) ) )
#define idx_obs(i,j) ((j) + Dy * (i) )

class Obstacle{
  /*This class is an abstract class, describing the implementation of an ostacle in the pipe*/
 public:
  Obstacle(int R);
  virtual ~Obstacle(); /*Classes will inherit from obstacle, so destructor must be virtual*/
  virtual void markSolidNodes(bool** isFluid) = 0;
  virtual void applyBoundary(double *fin, double*fout, int Dy) = 0;
  virtual void computeStress(double *fin, int Dy, double tau) = 0;
  double getDrag();

 protected:
  int m_R; /*Characteristic dimension for the obstacle*/
  double m_drag;
};

class squareObstacle: public Obstacle{
 public:
  squareObstacle(int x0, int y0, int L);
  ~squareObstacle();
  virtual void markSolidNodes(bool** isFluid);
  virtual void applyBoundary(double *fin, double*fout, int Dy);
  //virtual double computeDrag(double *fin, int Dy, double tau);
  virtual void computeStress(double *fin, int Dy, double tau);
  double getViscousTop();
  double getViscousBot();
  double getViscousFront();
  double getViscousRear();
  double getpFront();
  double getpRear();
 private:
  int m_x0, m_xmax;
  int m_y0, m_ymax;
  double m_viscousTop;
  double m_viscousBot;
  double m_viscousFront;
  double m_viscousRear;
  double m_pFront;
  double m_pRear;
};

class Grid: public Obstacle{
 public:
  Grid(int x0, int L, int Dy); /*Needs Dy to compute the nb of lamina composing the grid*/
  virtual void markSolidNodes(bool** isFluid);
  virtual void applyBoundary(double *fin, double*fout, int Dy);
  virtual double computeDrag(double *fin, int Dy, double tau);
  virtual void computeStress(double *fin, int Dy, double tau);
  
 private:
  int m_x0, m_nbLamina;
};
#endif
