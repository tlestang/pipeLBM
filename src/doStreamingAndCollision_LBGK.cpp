#include "pipeLBM.h"

void pipeLBM::doStreamingAndCollision_BGK()
{
  int c[9][2] = {{0,0}, {1,0}, {0,1}, {-1,0}, {0,-1}, {1,1}, {-1,1}, {-1,-1}, {1,-1}};
  double w[9]={4.0/9.0, 1.0/9.0, 1.0/9.0, 1.0/9.0, 1.0/9.0, 1.0/36.0, 1.0/36.0, 1.0/36.0, 1.0/36.0};

  double rho_, fk;
  double ux_, uy_; double omega = 1./m_tau; double eu, eueu, u2, feq;
  double omega1 = 1. - omega;
  double l, om, om1;
  int nx, ny;

  double coeff_forcing = 1. - 0.5*omega;
  double force_driving;
  for(int x=0;x<m_spgeStart;x++)
    {
      for(int y=0;y<m_Dy;y++)
	{
	  /*in this region the sponge layer is inactive*/
	  if(isFluid[x][y])
	    {
	      ux_ = ux[idx(x,y)]; uy_ = uy[idx(x,y)];
	      rho_ = rho[idx(x,y)];
	      u2 = -1.5*(ux_*ux_ + uy_*uy_);
	      for (int k=0;k<9;k++)
		{
		  eu = c[k][0]*ux_ + c[k][1]*uy_;
		  eueu = 4.5*eu*eu;
		  feq = w[k]*rho_*(1.0+3.0*eu+eueu+u2);
		  force_driving = w[k]*coeff_forcing*m_F0*(3.*(c[k][0]-ux_)+9.*c[k][0]*eu);
		  fin[IDX(x,y,k)]=fin[IDX(x,y,k)]*omega1+feq*omega+force_driving;
		  nx = (x + c[k][0]+m_Dx)%m_Dx; ny = (y + c[k][1]+m_Dy)%m_Dy;
		  fout[IDX(nx,ny,k)]=fin[IDX(x,y,k)];
		}
	    }
	}
    }
  for(int x=m_spgeStart;x<m_spgeEnd;x++)
    {
      l = (x-m_spgeStart)/(double)(m_spgeEnd-1 - m_spgeStart);
      om = (1-0.999*l*l)*omega;
      om1 = 1-om;
      for(int y=0;y<m_Dy;y++)
	{
	  /*in this region the sponge layer is active*/
	  if(isFluid[x][y])
	    {
	      ux_ = ux[idx(x,y)]; uy_ = uy[idx(x,y)];
	      rho_ = rho[idx(x,y)];
	      u2 = -1.5*(ux_*ux_ + uy_*uy_);
	      for (int k=0;k<9;k++)
		{
		  eu = c[k][0]*ux_ + c[k][1]*uy_;
		  eueu = 4.5*eu*eu;
		  feq = w[k]*rho_*(1.0+3.0*eu+eueu+u2);
		  force_driving = w[k]*coeff_forcing*m_F0*(3.*(c[k][0]-ux_)+9.*c[k][0]*eu);
		  fin[IDX(x,y,k)]= fin[IDX(x,y,k)]*om1+feq*om+force_driving;	  
		  /*Streaming*/
		  nx = (x + c[k][0]+m_Dx)%m_Dx; ny = (y + c[k][1]+m_Dy)%m_Dy;
		  fout[IDX(nx,ny,k)] = fin[IDX(x,y,k)];
		}
	    }
	}
    }
  for(int x=m_spgeEnd;x<m_Dx;x++)
    {
      om = 0.001*omega;
      om1 = 1-om;
      for(int y=0;y<m_Dy;y++)
	{
	  /*in this region the sponge layer is active*/
	  if(isFluid[x][y])
	    {
	      ux_ = ux[idx(x,y)]; uy_ = uy[idx(x,y)];
	      rho_ = rho[idx(x,y)];
	      u2 = -1.5*(ux_*ux_ + uy_*uy_);
	      for (int k=0;k<9;k++)
		{
		  eu = c[k][0]*ux_ + c[k][1]*uy_;
		  eueu = 4.5*eu*eu;
		  feq = w[k]*rho_*(1.0+3.0*eu+eueu+u2);
		  force_driving = w[k]*coeff_forcing*m_F0*(3.*(c[k][0]-ux_)+9.*c[k][0]*eu);
		  fin[IDX(x,y,k)]= fin[IDX(x,y,k)]*om1+feq*om+force_driving;	  
		  /*Streaming*/
		  nx = (x + c[k][0]+m_Dx)%m_Dx; ny = (y + c[k][1]+m_Dy)%m_Dy;
		  fout[IDX(nx,ny,k)] = fin[IDX(x,y,k)];
		}
	    }
	}
    }
}
