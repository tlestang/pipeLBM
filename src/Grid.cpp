#include "Obstacle.h"

Grid::Grid(int x0, int R, int Dy) : Obstacle(R), m_x0(x0)
{
  m_nbLamina = ((Dy-1)/R)/2;
}
void Grid::markSolidNodes(bool** isFluid)
{
  int yStart, yEnd;
  for(int i=0;i<m_nbLamina;i++)
    {
      yStart = 2*i*m_R + m_R/2;
      yEnd = yStart + m_R;
      for(int y=yStart;y<yEnd;y++)
	{
	  isFluid[m_x0][y]=false;
	}
    }
}
void Grid::applyBoundary(double *fin, double *fout, int Dy)
{
  /*Apply halfway BB to north and south wall, including corners*/
  int yStart,yEnd; int y0;
  for(int i=0;i<m_nbLamina;i++)
    {
      yStart = 2*i*m_R+m_R/2;
      yEnd = yStart + m_R;
      for(int y=yStart;y<yEnd;y++)
	{
	  /*inside the lamina*/
	   fout[IDX_OBS(m_x0-1,y,6)] = fin[IDX_OBS(m_x0-1,y,8)];
	   fout[IDX_OBS(m_x0-1,y,3)] = fin[IDX_OBS(m_x0-1,y,1)];
	   fout[IDX_OBS(m_x0-1,y,7)] = fin[IDX_OBS(m_x0-1,y,5)];

	   fout[IDX_OBS(m_x0+1,y,8)] = fin[IDX_OBS(m_x0+1,y,6)];
	   fout[IDX_OBS(m_x0+1,y,1)] = fin[IDX_OBS(m_x0+1,y,3)];
	   fout[IDX_OBS(m_x0+1,y,5)] = fin[IDX_OBS(m_x0+1,y,7)];
	}
      y0 = yStart-1;
      fout[IDX_OBS(m_x0-1,y0,7)] = fin[IDX_OBS(m_x0-1,y0,5)];
      fout[IDX_OBS(m_x0+1,y0,8)] = fin[IDX_OBS(m_x0+1,y0,6)];
      fout[IDX_OBS(m_x0,y0,4)] = fin[IDX_OBS(m_x0,y0,2)];
      y0 = yEnd;
      fout[IDX_OBS(m_x0-1,y0,6)] = fin[IDX_OBS(m_x0-1,y0,8)];
      fout[IDX_OBS(m_x0+1,y0,5)] = fin[IDX_OBS(m_x0+1,y0,7)];
      fout[IDX_OBS(m_x0,y0,2)] = fin[IDX_OBS(m_x0,y0,4)];
    }
}
double Grid::computeDrag(double *fin, int Dy, double tau)
{
  /*This method is not implemented yet*/
  return 1.0;
}
void Grid::computeStress(double *fin, int Dy, double tau)
{
  /*This method is not implemented yet*/
  m_drag = 1.0;;
}
